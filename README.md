PRTG Device Template for Sonoff Pow R2 Smart Switch
===================================================

This project contains all the files necessary to retrieve energy usage data from a Sonoff Pow R2 smart switch, running Tasmota firmware,
via a  [PRTG REST Custom Sensor](https://www.paessler.com/manuals/prtg/rest_custom_sensor).

The template was developed and tested with Tasmota firmware 6.4.0.3

To upload the custom firmware, a four pin header must be soldered to the the Sonoff device. This will invalidate the manufacturers warranty. The instructions for installing the header and flashing the software are identical to those in this video that describes how to 
modify a Sonoff S20 smart plug -  https://www.youtube.com/watch?v=OlSfq7SLqwE&list=PLlzpc0FQ9agiSpwByPf7822EqM0HlfWGs

Note - Sonoff devices are mains powered and therefore potentially dangerous. Before attempting this project, please read and understand the instructions and safety guidelines on the Tasmota project pages:
•	https://github.com/arendst/Sonoff-Tasmota 
•	https://github.com/arendst/Sonoff-Tasmota/wiki/Hardware-Preparation  
•	https://github.com/arendst/Sonoff-Tasmota/wiki/Sonoff-Pow-and-Pow-R2#push-result-using-telemetry

[Details on how to add the device to PRTG can be found in this blog post on Paesslers web site](https://blog.paessler.com/prtg-sonoff-smart-meter)

Download Instructions
=========================
 [A zip file containing all the files in the installation package can be downloaded directly using this link](https://gitlab.com/PRTG/Sensor-Scripts/Sonoff-Pow-R2/-/jobs/artifacts/master/download?job=PRTGDistZip)
.


Installation Instructions
=========================

Please refer to INSTALL.md

