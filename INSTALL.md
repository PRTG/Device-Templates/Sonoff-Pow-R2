Installation Instructions
=========================

Preparations

This custom sensor project has a standard directory structure:
All the files in the PRTG subdirectory needs to go into the PRTG program directory
(https://kb.paessler.com/en/topic/463-how-and-where-does-prtg-store-its-data).
This means, copy the files from "prtg" to "%programfiles(x86)%\PRTG Network Monitor" and the files will be copied into the correct locations in the subdirectories.

Please ** NOTE **, the destination "%programfiles(x86)%" is a "protected path " and requires elevated privileges.
(IE if you copy across the network, the "admin user may not have the correct drive mappings).

Instructions

Device Setup

To retrieve energy usage telemetry from the Sonoff Pow R2, it must first be flashed with Tasmota firmware. To do this, a four pin header must be soldered to the device's PCB. Instructions for how to do this can be found in this video: 

https://www.youtube.com/watch?v=OlSfq7SLqwE&list=PLlzpc0FQ9agiSpwByPf7822EqM0HlfWGs

The video describes how to modify a Sonoff S20 smart plug, but the process is exactly the same for the Sonoff Pow R2 smart switch.

The Tasmota firmware includes a large number of selectable parameters, some of which (such as WiFi settings) must be changed before uploadng the firmware to the device. The video, linked above, discusses some of the options. But full details can be found on the Tasmota project pages:

https://github.com/arendst/Sonoff-Tasmota
https://github.com/arendst/Sonoff-Tasmota/wiki

***WARNING*** - As Sonoff devices are mains (grid) powered, you should also read, understand and comply with the safety information prvided on the Tasmota project pages.

Once the device has been flashed, you should be able to connect to its web interface by pointing a browser to the device's IP address. If you defined login credentials in the firmware config, you will be prompted to enter these before the web interface opens.

Once connected, click Configuration – Configure Module. Then, from the dropdown Module Type, select Sonoff Pow R2 (43) and click Save. This enables the power usage telemetry on the device, and once it automatically restarts, the data should be visible on the main menu of the web interface.

Note that after the module type is changed to Sonoff Pow R2, the serial interface is no longer available for entering commands, because it is used to communicate with the power monitoring system.

PRTG Setup

If you haven't already done so, you should now download the Sonoff_Pow_R2 template zip file from the PRTG Device Templates GitLab repository. The downloaded file should be extracted and the contents copied to your PRTG server as described in the Preparations section, above.

In PRTG, you should add a new device for the Sonoff switch - give it a meaningful name, enter the IP address for the switch, and (optionally) select the Sonoff logo as the device tree symbol.

Next, add a REST Custom Sensor to the device. Again, provide a meaningful name. In the "REST Query" field, enter the API call:

/cm?cmnd=status%2010

If you defined login credentials in the firmware config, these must be included in the API query:

/cm?user=xxxxxxx&password=yyyyyyy&cmnd=status%2010

Where xxxxxxx is the username you defined in the firmware config, and yyyyyyy is the corresponding password.

In the Rest Configuration dropdown, select the Sonoff_Pow.template file.

After the first sensor poll, the channels will populate and shortly after, data will be visible. You can then create limits (thresholds) and assign notifications on any of the available channels. 